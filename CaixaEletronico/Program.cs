﻿using System;

namespace Caixa_Eletronico
{
    class Program
    {
        static void Main(string[] args)
        {
            
            // Desenvolver um programa que simule a entrega de notas quando um cliente efetuar um saque em um caixa eletrônico. Os requisitos básicos são os seguintes:
            // Entregar o menor número de notas;
            // É possível sacar o valor solicitado com as notas disponíveis;
            // Saldo do cliente infinito;
            // Quantidade de notas infinito (pode-se colocar um valor finito de cédulas para aumentar a dificuldade do problema);
            // Notas disponíveis de R$ 100,00; R$ 50,00; R$ 20,00 e R$ 10,00
            

            //Pegar o valor digitado pelo usuário

            int [] notas = {100, 50, 20, 10};
            int valorNotas = 0;

            System.Console.WriteLine("Digite o valor de saque (múltiplos de 10)");
            int valorSaque = int.Parse(Console.ReadLine());

            if (valorSaque % 10 == 0){

                System.Console.WriteLine("Você receberá as seguintes notas:");
                for (int i = 0; i < notas.Length; i++){
                    while(valorNotas < valorSaque && valorSaque -valorNotas >= notas[i]){
                        valorNotas += notas[i];
                        System.Console.WriteLine(notas[i]);
                    }
                }//fim for
                

            }else{
                System.Console.WriteLine("Não será possível realizar este saque, pois este caixa dipõe apenas de notas de R$ 100,00; R$ 50,00; R$ 20,00 e R$ 10,00");
            } 
            
        }
    }
}
