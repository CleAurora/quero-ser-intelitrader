﻿using System;

namespace EstatisticaSimples
{
    class Program
    {
        static void Main(string[] args)
       {
             // Sua tarefa é processar uma seqüência de números inteiros para determinar as seguintes estatísticas:
            // Valor mínimo
            // Valor máximo
            // Número de elementos na seqüência
            // Valor médio
            System.Console.WriteLine("Entre com a quantidade de números da sua sequência");
            int tamanho = int.Parse(Console.ReadLine());
            int[] sequencia = new int[tamanho];
            int soma = 0;

            System.Console.WriteLine("Entre com os números desejados para a sequência");

            for (int i = 0; i < sequencia.Length; i++)
            {
                System.Console.WriteLine($"Entre com o {i+1}o número da seguência");
                sequencia[i] = int.Parse(Console.ReadLine());
                soma += sequencia[i];
            }
            Array.Sort(sequencia);
            int media = soma/(tamanho);
            

            System.Console.WriteLine($"O valor mínimo é {sequencia[0]}");
            System.Console.WriteLine($"O valor máximo é {sequencia[tamanho-1]}");
            System.Console.WriteLine($"O número de elementos da sequência é {tamanho}");
            System.Console.WriteLine($"O valor médio dos números dessa seQuência é {media}");






        }
    }
}
