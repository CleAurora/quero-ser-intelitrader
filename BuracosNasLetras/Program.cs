﻿using System;

namespace BuracosNasLetras
{
    class Program
    {
        static void Main(string[] args)
        {
            //0 = C E F G H I J L M N S T U V X Z K W Y
            //1 = A D O P Q R 
            //2 = B 

            int buracos = 0;

            System.Console.WriteLine("Escreva seu texto");
            string texto = Console.ReadLine().ToUpper();
            char [] letras = texto.ToCharArray();

            for (int i = 0; i < letras.Length; i++)
            {
                if (letras[i] == 'B'){
                    buracos+=2;
                }else if (letras[i] == 'A' || letras[i] == 'D' || letras[i] == 'O' || letras[i] == 'P' || letras[i] == 'Q' || letras[i] == 'R'){
                    buracos+=1;
                }
            }

            System.Console.WriteLine($"Neste texto há {buracos} buracos!");
            
        }
    }
}
