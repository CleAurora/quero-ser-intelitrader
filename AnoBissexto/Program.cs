﻿using System;

namespace AnoBissexto
{
    class Program
    {
        static void Main(string[] args)
        {
            //Escrever uma função que determina se um determinado ano informado é bissexto ou não.
            System.Console.WriteLine("Entre com o ano para saber se é bissexto ou não");
            int ano = int.Parse(Console.ReadLine());

            if(ano%400==0){
                System.Console.WriteLine($"{ano} é um ano bissexto");
            }else if(ano%100==0){
                System.Console.WriteLine($"{ano} não é um ano bissexto");
            }else if (ano%4==0){
                System.Console.WriteLine($"{ano} é um ano bissexto");
            }else{
                System.Console.WriteLine($"{ano} não é um ano bissexto");
            }
            


        }
    }
}
